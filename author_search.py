import requests
from lxml import html
import re
import json

def search_author(search_name : str, sort : int):
    """returns the first page of author search"""
    response = requests.get(url='https://sinta.kemdikbud.go.id/authors',
                            params={
                                    'q' : search_name,
                                    'page' : 1,
                                    'sort' : sort
                            })
    
    main_parser = html.fromstring(response.text)
    author_elements = main_parser.xpath('//body//div[@class="content"]//div[contains(@class, "list-item")]')

    search_result = []
    for author_element in author_elements:
        author_name = author_element.xpath('.//div[@class="profile-name"]/a/text()')[0].strip()
        author_sinta_id = re.search(r'SINTA ID\s:\s(\d+)',str(author_element.xpath('.//div[@class="profile-id"]/text()'))).group(1)
        author_sinta_link = author_element.xpath('.//div[@class="profile-name"]/a/@href')[0].strip()
        author_affiliation = author_element.xpath('.//div[@class="profile-affil"]/a/text()')[0].strip()
        author_affiliation_link = author_element.xpath('.//div[@class="profile-affil"]/a/@href')[0].strip()
        author_dept = author_element.xpath('.//div[@class="profile-dept"]/a/text()')[0].strip()
        author_dept_link = author_element.xpath('.//div[@class="profile-dept"]/a/@href')[0].strip()

        subjects = author_element.xpath('.//div[contains(@class, "meta-side")]//ul[contains(@class, "subject-list")]//a/text()')
        subject_links = author_element.xpath('.//div[contains(@class, "meta-side")]//ul[contains(@class, "subject-list")]//a/@href')

        metric_names = [element.strip() for element in author_element.xpath('.//div[contains(@class, "meta-side")]//table//td[1]/div/text()')]
        metric_values = [int(element.strip()) for element in author_element.xpath('.//div[contains(@class, "meta-side")]//table//td[3]/div/text()')]

        sinta_score_name = [element.strip() for element in author_element.xpath('.//div[contains(@class, "bottom row")]/div/div[@class="pr-txt"]/text()')]
        sinta_score_num = [int(element.strip().replace(".", ""))   for element in author_element.xpath('.//div[contains(@class, "bottom row")]/div/div[@class="pr-num"]/text()')]

        json_data = {
            "name" : author_name,
            "affiliation" : {
                "name" : author_affiliation,
                "affiliation_link" : author_affiliation_link
            },
            "dept" :{
                "name" : author_dept,
                "dept_link" : author_dept_link
            },
            "sinta_id" : author_sinta_id,
            "sinta_link" : author_sinta_link,
            "subjects" : [{"name": subject, "link": link} for subject, link in zip(subjects, subject_links)],
            "metrics" : dict(zip(metric_names, metric_values)),
            "sinta_score": dict(zip(sinta_score_name, sinta_score_num))

        }
        
        search_result.append(json_data)
    return json.dumps(search_result, indent=3)

if __name__ == "__main__":
    print(search_author('wahyudi sutopo', 2))
