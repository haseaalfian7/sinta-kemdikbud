from lxml import html
from re import search
import requests
import json

def sinta_research_detail(sinta_id : str):
    """returns a list of research details of the authors with the given sinta_id"""
    #TODO: gather data directly from Scopus, WOS, Garuda, Scholar, and RAMA

    SINTA_AUTHOR_BASE_URL = 'https://sinta.kemdikbud.go.id/authors/profile/'
    sinta_author_url = SINTA_AUTHOR_BASE_URL + sinta_id

    view_params = [ 'wos','scopus',
                    # 'googlescholar', 'garuda', 'rama'
                    ]

    article_detail = {}
    for view in view_params:
        author_page = requests.get(sinta_author_url,
                                   params={
                                       "view" : view
                                   })
        view_parser = html.fromstring(author_page.text)
        article_elements = view_parser.xpath('//div[@class="content-box"]//div[@class="profile-article"]//div[contains(@class, "list-item")]')
        
        article_list = []
        for article in article_elements:
            article_dict = {}
            # GENERAL
            article_dict['ar_name'] = article.xpath('.//div[@class="ar-title"]/a/text()')[0].strip()
            article_dict['ar_link'] = article.xpath('.//div[@class="ar-title"]/a/@href')[0].strip()
            article_dict['ar_pub'] = article.xpath('.//div[@class="ar-meta"]/a[@class="ar-pub" and i]/text()')[0].strip()
            article_dict['ar_pub_link'] = article.xpath('.//div[@class="ar-meta"]/a[@class="ar-pub" and i]/@href')[0].strip()
            article_dict['au_order'] = search(r":(.*)",article.xpath('.//div[@class="ar-meta"]/a[contains(translate(text(), "ORDER", "order"), "order")]/text()')[0]).group(1).strip()
            article_dict['ar_year'] = article.xpath('.//div[@class="ar-meta"]/a[@class="ar-year"]/text()')[0].strip()
            article_dict['ar_cited'] = search(r'(\d+)',article.xpath('.//div[@class="ar-meta"]/a[@class="ar-cited"]/text()')[0].strip()).group(1)

            #SCOPUS
            try:
                article_dict['ar_quartile'] = article.xpath('.//div[@class="ar-meta"]/a[@class="ar-quartile"]/text()')[0].strip()
            except:
                pass
            try:
                article_dict['ar_creator'] = search(r'Creator\s*:\s*(.*)',article.xpath('.//div[@class="ar-meta"]/a[contains(translate(text(), "CREATOR", "creator"), "creator")]/text()')[0]).group(1)
            except:
                pass

            # WOS
            try:
                article_dict['ar_pub2'] = article.xpath('.//div[@class="ar-meta"]/a[@class="ar-pub" and not(i)]/text()')[0].strip()
            except:
                pass
            try:
                article_dict['ar_authors'] = search(r':(.*)',article.xpath('.//div[@class="ar-meta"]/a[contains(translate(text(), "AUTHORS", "authors"), "authors")]/text()')[0]).group(1).strip() # WOS
            except:
                pass
            try:
                article_dict['scopus_idx'] = article.xpath('.//div[@class="ar-meta"]/span[contains(@class,"scopus-indexed")]/span/text()')[0].strip() #WOS
            except:
                pass
            try:
                article_dict['doi'] = search(r':(.*)',article.xpath('.//div[@class="ar-meta"]/a[@class="ar-sinta"]/text()')[0]).group(1).strip() #WOS
            except:
                pass
            try:
                article_dict['doi_link'] = article.xpath('.//div[@class="ar-meta"]/a[@class="ar-sinta"]/@href')[0].strip() #WOS
            except:
                pass
            article_list.append(article_dict)
        article_detail[view] = article_list
    return json.dumps(article_detail, indent=3)

if __name__ == '__main__':
    print(sinta_research_detail('5974504'))


    